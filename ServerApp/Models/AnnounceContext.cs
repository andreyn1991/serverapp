﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ServerApp.Models
{
    public sealed class AnnounceContext : DbContext
    {
        public DbSet<Bulletin> BulletinBoard { get; set; }
        public DbSet<User> Users { get; set; }

        public AnnounceContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
