﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApp.Models
{
    public class User
    {
        [Required]
        public Guid UserId { get; set; }
        public string Name { get; set; }
    }
}
