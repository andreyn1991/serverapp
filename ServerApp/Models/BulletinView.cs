﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerApp.Models
{
    public class BulletinView
    {
        public BulletinView(int number, string user, string text, int rating, DateTime created)
        {
            Number = number;
            User = user;
            Text = text;
            Rating = rating;
            Created = created;
        }

        public int Number { get;}
        public string User { get;}
        public string Text { get;}
        public int Rating { get;}
        public DateTime Created { get;}
    }
}
