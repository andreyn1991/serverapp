﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServerApp.Models;

namespace ServerApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BulletinsController : ControllerBase
    {
        private readonly AnnounceContext _context;

        public BulletinsController(AnnounceContext context)
        {
            _context = context;
        }

        // GET: api/Bulletins
        [HttpGet]
        public List<BulletinView> GetBulletinBoard()
        {
            return _context.BulletinBoard.Join(_context.Users, p => p.UserId, t => t.UserId,
                (p, t) => new BulletinView(p.Number, t.Name, p.Text, p.Rating, p.Created)).ToList();
        }

        // GET: api/Bulletins/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBulletin([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bulletin = await _context.BulletinBoard.FindAsync(id);

            if (bulletin == null)
            {
                return NotFound();
            }

            return Ok(bulletin);
        }

        // PUT: api/Bulletins/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBulletin([FromRoute] Guid id, [FromBody] Bulletin bulletin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bulletin.Id)
            {
                return BadRequest();
            }

            _context.Entry(bulletin).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BulletinExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Bulletins
        [HttpPost]
        public async Task<IActionResult> PostBulletin([FromBody] Bulletin bulletin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.BulletinBoard.Add(bulletin);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBulletin", new { id = bulletin.Id }, bulletin);
        }

        // DELETE: api/Bulletins/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBulletin([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bulletin = await _context.BulletinBoard.FindAsync(id);
            if (bulletin == null)
            {
                return NotFound();
            }

            _context.BulletinBoard.Remove(bulletin);
            await _context.SaveChangesAsync();

            return Ok(bulletin);
        }

        private bool BulletinExists(Guid id)
        {
            return _context.BulletinBoard.Any(e => e.Id == id);
        }
    }
}